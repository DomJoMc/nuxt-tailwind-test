module.exports = {
    theme: {
        extend: {
            colors: {
                'default': '#1B1D20',
                'darkblue': '#212B37',
                'orange': '#FF7800'
            },
            fontFamily: {
                'messinalight': 'MessinaLight',
                'messinaregular': 'MessinaRegular',
                'messinabook': 'MessinaBook'
            },
            fontSize: {
                'title-lg': '3.75rem', // 60px,
                'title-md': '2.8125rem' // 45px
            },
            lineHeight: {
                'default-sm': '2.1875rem', // 35px
                'default': '2.8125rem' // 45px
            },
            width: {
                '10%': '10%',
                '65%': '65%',
                '70%': '70%'
            },
            inset: {
                '50px': '50px'
            }
        },
    },
    variants: {},
    plugins: []
}
