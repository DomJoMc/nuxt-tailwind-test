Dept Frontend test by Dom McLaughlin.

This was my first time using both Nuxt and Tailwind CSS so apologies for any bad code practices. Having used Vue in the past for various projects, Nuxt was pretty easy to pick up. Tailwind is different to what I'm used to but in terms of the level of customisation it's very similar to how I would use SCSS to edit and create Bootstrap 4 variables.

I do prefer Bootstrap 4 as a CSS framework but Tailwind is definitely something I will take a look at more and use in future projects.

I've spent around 6 hours in total over 5 days on this project, this includes learning the basics of Nuxt and Tailwind and also debugging an error with the `vue-slick` library caused by SSR.

I had planned to use the `vue-in-viewport-directory` library to target certain elements on scroll and include some nice animations however I've sadly not had the time.

When looking at the 'What we offer" section I took this as being a carousel and decided to use Slick Slider. On the design I noticed only the first element was open. I wasn't sure if this was intentional but in the event it was, I thought about how it would work and some ideas included:

1. The carousel would loop automatically and only the first item would show content.
2. On hover of the closed items, the content would fade in.

In the end I decided to show the content for all items for simplicity but this is something I would have confirmed with the designer.