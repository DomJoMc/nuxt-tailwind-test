const path = require('path');

export default {
    mode: 'universal',
    head: {
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
        ],
    },
    css: [
        '~/assets/css/tailwind.css'
    ],
    modules: [
        'nuxt-purgecss'
    ],
    plugins: [
        {src: '~/plugins/vue-slick', mode: 'client'}
    ],
    build: {
        extractCSS: true,
        postcss: {
            plugins: {
                tailwindcss: path.resolve(__dirname, './tailwind.config.js')
            }
        },
        transpile: [
            'consola'
        ]
    }
}